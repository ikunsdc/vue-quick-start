# Vue快速入门   Vue quick start 

#### Vue介绍
  &emsp;&emsp;Vue.js 是一个JavaScript框架  
  &emsp;&emsp;Vue作者尤雨溪  官方文档：https://cn.vuejs.org  
  &emsp;&emsp;Vue.js 的核心是一个允许采用简洁的模板语法来声明式地将数据渲染进 DOM 的系统。



#### 仓库介绍
  &emsp;&emsp;本仓库是一套快速入门Vue教程，包括本人在快速入门Vue中的学习笔记|全部代码|视频，适合短时间内快速入门Vue.  

  &emsp;&emsp;教程：https://www.bilibili.com/video/BV12J411m7MG?from=search&seid=620123443536343094    
  &emsp;&emsp;笔记：https://gitee.com/ikunsdc/vue-quick-start/blob/master/VUE%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8.pdf    
  &emsp;&emsp;代码：https://gitee.com/ikunsdc/vue-quick-start    


#### 教程安排

1.  第一章：Vue基础
2.  第二章：本地应用
3.  第三章：网络应用
4.  第四章：综合应用

#### 教程|笔记详细内容

&emsp;&emsp;VUE快速入门  
&emsp;&emsp;&emsp;&emsp;开发工具  
&emsp;&emsp;&emsp;&emsp;课程安排  
&emsp;&emsp;第一章：Vue基础  
&emsp;&emsp;&emsp;&emsp;Vue基础-简介  
&emsp;&emsp;&emsp;&emsp;Vue基础-第一个Vue程序  
&emsp;&emsp;&emsp;&emsp;Vue基础-el挂载点  
&emsp;&emsp;&emsp;&emsp;Vue基础-data数据对象  
&emsp;&emsp;第二章：本地应用  
&emsp;&emsp;&emsp;&emsp;本地应用-V-text指令  
&emsp;&emsp;&emsp;&emsp;本地应用-V-html指令  
&emsp;&emsp;&emsp;&emsp;本地应用-V-on基础  
&emsp;&emsp;&emsp;&emsp;本地应用案例-计数器  
&emsp;&emsp;&emsp;&emsp;本地应用-V-show  
&emsp;&emsp;&emsp;&emsp;本地应用-V-if  
&emsp;&emsp;&emsp;&emsp;本地应用-V-bind  
&emsp;&emsp;&emsp;&emsp;本地应用案例-图片切换器  
&emsp;&emsp;&emsp;&emsp;本地应用-v-for  
&emsp;&emsp;&emsp;&emsp;本地应用-v-on补充  
&emsp;&emsp;&emsp;&emsp;本地应用-v-model补充  
&emsp;&emsp;&emsp;&emsp;小黑记事本-介绍  
&emsp;&emsp;&emsp;&emsp;小黑记事本-新增  
&emsp;&emsp;&emsp;&emsp;小黑记事本-删除  
&emsp;&emsp;&emsp;&emsp;小黑记事本-统计  
&emsp;&emsp;&emsp;&emsp;小黑记事本-清空  
&emsp;&emsp;&emsp;&emsp;小黑记事本-隐藏  
&emsp;&emsp;第三章：网络应用  
&emsp;&emsp;&emsp;&emsp;axios基本使用  
&emsp;&emsp;&emsp;&emsp;axios+vue  
&emsp;&emsp;&emsp;&emsp;案例：天知道-介绍  
&emsp;&emsp;&emsp;&emsp;案例：天知道-回车查询  
&emsp;&emsp;&emsp;&emsp;案例：天知道-点击查询  
&emsp;&emsp;第四章：综合应用  
&emsp;&emsp;&emsp;&emsp;综合应用-介绍  
&emsp;&emsp;&emsp;&emsp;综合应用-音乐查询  
&emsp;&emsp;&emsp;&emsp;综合应用-音乐播放  
&emsp;&emsp;&emsp;&emsp;综合应用-歌曲封面  
&emsp;&emsp;&emsp;&emsp;综合应用-歌曲评论  
&emsp;&emsp;&emsp;&emsp;综合应用-播放动画  
&emsp;&emsp;&emsp;&emsp;综合应用-播放MV  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 学前基础

&emsp;&emsp;&emsp;&emsp;HTML  
&emsp;&emsp;&emsp;&emsp;CSS  
&emsp;&emsp;&emsp;&emsp;JavaScript  
&emsp;&emsp;&emsp;&emsp;AJAX  





&nbsp;&nbsp;&nbsp;
#### Created By ikunsdc 2021/02/13

